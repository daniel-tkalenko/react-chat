import React from 'react';
import StartScreen from './StartScreen/index';

export default function App() {
  return (
    <div className="App">
      <StartScreen />
    </div>
  );
}
