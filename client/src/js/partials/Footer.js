import React from 'react';

export default function Footer() {
  return (
    <nav className="footer navbar navbar-light bg-light">
      <p className="navbar-brand copyright">BSA</p>
    </nav>
  );
}
